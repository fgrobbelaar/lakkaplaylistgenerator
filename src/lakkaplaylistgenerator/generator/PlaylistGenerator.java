/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lakkaplaylistgenerator.generator;

import java.io.BufferedWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lakkaplaylistgenerator.mainui.SettingsUI;

/**
 *
 * @author Francois
 */
public class PlaylistGenerator {
    SettingsUI ui;
    
    public PlaylistGenerator(SettingsUI ui) {
        this.ui = ui;
    }
    
    public boolean generateLakkaPlaylist(File romDirectory, File datFile,
                                         String lakkaCorePath, String lakkaRomPath,
                                         File playlistFileToGenerate) {
        StringBuffer playlistFile = new StringBuffer();
        List<String> listOfGameNames;
        
        // Load the DAT file and get all the game nodes
        NodeList xmlGameList = loadXMLFile(datFile);
        if (xmlGameList == null || xmlGameList.getLength() < 1) {
            return false;
        }
        
        log("INFO: Processing roms files");
        
        // Figure out the names of the games we want in the playlist and which roms they map to
        File[] filesInDirectory = romDirectory.listFiles();
        Map<String, String> gameMappings = getGameMappings(filesInDirectory, xmlGameList);
        
        // Get a sorted list of game names (case-insensitive)
        listOfGameNames = new ArrayList(gameMappings.keySet());
        Collections.sort(listOfGameNames, String.CASE_INSENSITIVE_ORDER);
        
        log("INFO: Found " + listOfGameNames.size() + " rom files matching entries in DAT file");
        
        // Generate an in-memory version of the playlist file
        log("INFO: Generating playlist");
        for (String gameName : listOfGameNames) {
            // Line 1: Full path to rom file including extension
            playlistFile.append(lakkaRomPath).append(gameMappings.get(gameName)).append("\n");
            
            // Line 2: Name of game
            playlistFile.append(gameName).append("\n");
            
            // Line 3: Full path to emulation core
            playlistFile.append(lakkaCorePath).append("\n");
            
            // Line 4: The display name of the core, this doesn't really matter, so we'll derive a
            //  name based on the path to the core
            playlistFile.append(getCoreName(lakkaCorePath)).append("\n");
            
            // Line 5: Link to the database entry, we don't know this so we can simply use the word
            //  'DETECT' to tell RetroArch to figure it out for us
            playlistFile.append("DETECT\n");
            
            // Line 6: The filename of the playlist
            playlistFile.append(playlistFileToGenerate.getName()).append("\n");
        }
        
        // Remove the final newline character from the playlist file
        if (playlistFile.toString().length() > 0) {
            playlistFile.deleteCharAt(playlistFile.toString().length() - 1);
        }
        
        log("INFO: Writing playlist to file: " + playlistFileToGenerate.getAbsolutePath());
        
        saveToFile(playlistFileToGenerate, playlistFile);
        
        log("INFO: Processing finished");
        
        return true;
    }

    private Map<String, String> getGameMappings(File[] roms, NodeList gameList) {
        HashMap<String, String> gameMapping = new HashMap<String, String>();
        
        for (File rom : roms) {
            if (!rom.isFile()) {
                continue;
            }
            
            if (rom.getName().startsWith(".") ||
                rom.getName().equalsIgnoreCase("thumbs.db") ||
                rom.getName().endsWith(".chd")) {
                continue;
            }
            
            if (gameIsBIOS(rom, gameList)) {
                log("INFO: Skipped BIOS rom: " + rom.getName());
                continue;
            }
            
            String gameName = getGameName(rom, gameList);
            
            if (gameName != null) {
                gameMapping.put(gameName, rom.getName());
            }
        }
        
        return gameMapping;
    }
    
    private NodeList loadXMLFile(File xmlFile) {
        try {
            log ("INFO: Loading DAT file: " + xmlFile.getAbsolutePath());
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList gameList = doc.getElementsByTagName("game");
            
            log ("INFO: DAT file loaded successfully (" + gameList.getLength() + " game entries)");
            
            return gameList;
        } catch (Exception ex) {
            log("ERROR: Could not parse DAT file: " + ex.getMessage());
            return null;
        }
    }
    
    private String getGameName(File rom, NodeList gameList) {
        String romName = rom.getName();
        
        // trim extension
        if (romName.lastIndexOf('.') > 1) {
            romName = romName.substring(0, romName.lastIndexOf('.'));
        }
        
        for (int i = 0 ; i < gameList.getLength(); i++) {
            Node currentGameNode = gameList.item(i);
            if (currentGameNode.getNodeType() == Node.ELEMENT_NODE) {
                Element game = (Element) currentGameNode;
                if (romName.equals(game.getAttribute("name"))) {
                    // We found a matching rom in the DAT file
                    Node descriptionNode = game.getElementsByTagName("description").item(0);
                    if (descriptionNode != null) {
                        // The entry has a proper description, so we can use it as the game name
                        return descriptionNode.getTextContent();
                    }
                }
            }
        }
        
        // Could not find a matching rom in the dat file, so log it and return the file-based name
        log("WARNING: Rom not found in DAT file: " + romName);
        return null;
    }
    
    private boolean gameIsBIOS(File rom, NodeList gameList) {
        String romName = rom.getName();
        
        // trim extension
        if (romName.lastIndexOf('.') > 1) {
            romName = romName.substring(0, romName.lastIndexOf('.'));
        }
        
        for (int i = 0 ; i < gameList.getLength(); i++) {
            Node currentGameNode = gameList.item(i);
            if (currentGameNode.getNodeType() == Node.ELEMENT_NODE) {
                Element game = (Element) currentGameNode;
                if (romName.equals(game.getAttribute("name"))) {
                    return ("yes".equalsIgnoreCase(game.getAttribute("isbios")));
                }
            }
        }
        
        return false;
    }
    
    String getCoreName(String corePath) {
        String result = corePath;
        
        if (result.lastIndexOf('/') > 0) {
            result = result.substring(result.lastIndexOf('/') + 1);
        }
        
        if (result.lastIndexOf('.') > 0) {
            result = result.substring(0, result.lastIndexOf('.'));
        }
        
        return result;
    }
    
    private void saveToFile(File playlistFileToGenerate, StringBuffer playListFile) {
        try {
            BufferedWriter bwr = new BufferedWriter(new FileWriter(playlistFileToGenerate));
            bwr.write(playListFile.toString());
            bwr.flush();
            bwr.close();
        } catch (IOException ex) {
            log("ERROR: Could not write to file: " + ex.getMessage());
        }
    }
    
    private void log(String text) {
        if (ui != null) {
            ui.log(text);
        }
    }
}
